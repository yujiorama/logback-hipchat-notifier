# 概要

* logback の appender です
* [hipchat の V2 API](https://www.hipchat.com/docs/apiv2) を使います
* [room へ notification します](https://www.hipchat.com/docs/apiv2/method/send_room_notification)

### 使い方 ###

* プロジェクトをビルドして maven のローカルリポジトリにインストール
* 使用するアプリの pom か build.gradle に dependency を追加
* logback.xml に appender を追加
    * API トークンは[ここから取得できる](https://www.hipchat.com/account/api)

~~~~
	pom.xml
	<dependency>
	<groupId>com.github.yujiorama</groupId>
	<artifactId>logback-hipchat-notifier</artifactId>
	<version>0.1</version>
	</dependency>
~~~~

~~~~
	logback.xml
	<appender name="hipchat" class="com.github.yujiorama.logback.appender.HipchatNotifyAppender">
	<serviceUrl>https://api.hipchat.com/</serviceUrl>
	<apiAccessToken>API_ACCESS_TOKEN_HERE</apiAccessToken>
	<roomName>ROOM_NAME_HERE</roomName>
	<color>red</color>
	<notify>true</notify>
	<messageFormat>text</messageFormat>
	<messagePrefix></messagePrefix>
	<messageSuffix></messageSuffix>
	</appender>
~~~~

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
