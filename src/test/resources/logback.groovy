import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.status.OnConsoleStatusListener

import java.nio.charset.Charset

statusListener(OnConsoleStatusListener)
scan("10 minutes")

def defaultCharset = Charset.forName("UTF-8")
def defaultPattern = "%date [%highlight(%-5level)] [%thread] [%cyan(%logger{32})] - %message%n"

appender("CONSOLE", ConsoleAppender) {

    encoder(PatternLayoutEncoder) {
        charset = defaultCharset
        pattern = defaultPattern
    }

}
