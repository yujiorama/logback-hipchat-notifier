package com.github.yujiorama.logback.appender

import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.AppenderBase
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method

class HipchatNotifyAppender extends AppenderBase<ILoggingEvent> {

    private String serviceUrl = "http://api.hipchat.com/v2/"

    private String apiAccessToken

    private String roomName

    private String color = "random"

    private boolean notify = false

    private String messageFormat = "html"

    private String messagePrefix

    private String messageSuffix

    String getServiceUrl() {
        return serviceUrl
    }

    void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl
    }

    String getApiAccessToken() {
        return apiAccessToken
    }

    void setApiAccessToken(String apiAccessToken) {
        this.apiAccessToken = apiAccessToken
    }

    String getRoomName() {
        return roomName
    }

    void setRoomName(String roomName) {
        this.roomName = roomName
    }

    String getColor() {
        return color
    }

    void setColor(String color) {
        this.color = color
    }

    boolean isNotify() {
        return notify
    }

    void setNotify(boolean notify) {
        this.notify = notify
    }

    String getMessageFormat() {
        return messageFormat
    }

    void setMessageFormat(String messageFormat) {
        this.messageFormat = messageFormat
    }

    String getMessagePrefix() {
        return messagePrefix
    }

    void setMessagePrefix(String messagePrefix) {
        this.messagePrefix = messagePrefix
    }

    String getMessageSuffix() {
        return messageSuffix
    }

    void setMessageSuffix(String messageSuffix) {
        this.messageSuffix = messageSuffix
    }

    @Override
    protected void append(ILoggingEvent eventObject) {
        def httpHandle = new HTTPBuilder(getServiceUrl())
        httpHandle.request(Method.POST, ContentType.JSON) {
            uri.path = "/v2/room/${roomName}/notification"
            headers.'Authorization' = "Bearer ${apiAccessToken}"

            body = [
                    color         : getColor(),
                    message       : getMessagePrefix() + eventObject.formattedMessage.substring(0, 120) + getMessageSuffix(),
                    notify        : isNotify(),
                    message_format: getMessageFormat()
            ]

            response.success = { r, json ->
                // nothing to do
            }

            response.failure = { r ->
                // nothing to do
            }
        }
    }

}
